class MyList():
    def __init__(self):
        self.my_list = []

    def AddItem(self, val):
        self.my_list.append(val)

    def getList(self):
        return self.my_list

    def getSlicedList1(self, index1, index2):
        return self.my_list[index1:index2]

    def getSlicedList2(self, index):
        return self.my_list[index:]

    def editList(self, index, val):
        self.my_list[index] = val

    def deleteItem(self, index):
        del self.my_list[index]

    def concateList(self, newList):
        self.my_list = self.my_list + newList

    def lenOfList(self):
        return len(self.my_list)

    def repetitionOfList(self, val):
        self.my_list = self.my_list*val

    def findItem(self, val):
        return val in self.my_list

    def iterateList(self):
        l = []
        for x in self.my_list:
            l.append(x)
        return l

    def getValueAt(self, index):
        return self.my_list[index]

    def clearList(self):
        self.my_list.clear()
        # self.my_list = []

    def average(self):
        total = 0
        for i in self.my_list:
            total += i
        return total/len(self.my_list)

    def get_even_odd(self):
        even = []
        odd = []
        for i in self.my_list:
            if i % 2 == 0:
                even.append(i)
            else:
                odd.append(i)
        return even, odd
