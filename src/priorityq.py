# A simple implementation of Priority Queue
# using Queue.
class PriorityQueue():
    def __init__(self):
        self.queue = []

    def __str__(self):
        return ' '.join([str(i) for i in self.queue])

    def getQueue(self):
        return self.queue

    # for checking if the queue is empty
    def isEmpty(self):
        return len(self.queue) == 0

    # for inserting an element in the queue
    def insert(self, data):
        self.queue.append(data)

    # for popping an element based on Priority
    def delete(self):
        """
        try:
            max_ = 0
            for i in range(len(self.queue)):
                if self.queue[i] > self.queue[max_]:
                    max_ = i
            item = self.queue[max_]
            del self.queue[max_]
            return item
        except IndexError:
            print()
            exit()
        """
        max_ = max(self.queue)
        index = self.queue.index(max_)
        del self.queue[index]
        return max_
