def basics_of_python():
    try:
        a = 10
        b = 20
        c = a+b
        #print(a+b)
        #print(a-b)
        #print(a/b)
        #print(a*b)
        return True
    except ZeroDivisionError:
        print("a/b result in 0")
        return False
    else:
        print(c)

def type_of_variable(in1):
    print(type(in1))
    return type(in1)

def basics_of_length(in1):
    print(len(in1))
    return len(in1)

def basics_of_cast(in1, to_type):
    ret = 0
    if to_type == "int":
        ret = int(in1)
    elif to_type == "float":
        ret = float(in1)
    elif to_type == "str":
        ret = str(in1)
    elif to_type == "bool":
        ret = bool(in1)
    else:
        ret = in1
    return ret

def basics_of_keywords(key):
    ret = False
    keyword_list = ['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class',
                    'continue', 'def', 'del', 'elif', 'else', 'except', 'finally',
                    'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda',
                    'nonlocal', 'not', 'or', 'pass', 'raise', 'return',
                    'try', 'while', 'with', 'yield']

    if key in keyword_list:
        ret = True
    return ret
