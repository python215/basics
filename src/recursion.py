def sum_naturals(num):
    return num if num <= 1 else num + sum_naturals(num-1)

def sum_digits(num):
    return num if num <= 1 else num%10+sum_digits(num//10)

def get_list_from_num_1(num, l=[]):
    # l.clear()
    if num == 0:
        return l
    l.append(num)
    return get_list_from_num_1(num-1, l)

def get_list_from_1_num(num, l=[]):
    if num != 0:
        get_list_from_1_num(num-1, l)
        l.append(num)
    return l

def factorial(num):
    if num <= 1:
        return num
    return num * factorial(num-1)

def Palindrome(val):
    rev = 0
    rem = 0
    while val > 0:
        rem = val % 10
        rev = rev * 10 + rem
        val //= 10
    return rev
