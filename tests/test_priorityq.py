import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from priorityq import PriorityQueue


def test_priorityq():
    q = PriorityQueue()
    q = PriorityQueue()
    q.insert(12)
    q.insert(1)
    q.insert(14)
    q.insert(7)
    print(q)
    l = str(q)
    print(l)
    q_ = q.getQueue()
    assert q_ == [12, 1, 14, 7]
    assert q.isEmpty() == False
    assert q.delete() == 14
    assert q.delete() == 12
    assert q.delete() == 7
    assert q.isEmpty() == False
    assert q.delete() == 1
    assert q.isEmpty() == True
