import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

import recursion

def test_recursion():

    assert recursion.sum_naturals(5) == 15
    assert recursion.sum_naturals(0) == 0
    assert recursion.sum_naturals(1) == 1
    assert recursion.sum_naturals(10) == 55

    assert recursion.sum_digits(123) == 6
    assert recursion.sum_digits(1235) == 11
    assert recursion.sum_digits(0) == 0
    assert recursion.sum_digits(1) == 1

    assert recursion.get_list_from_num_1(5) == [5, 4, 3, 2, 1]
    assert recursion.get_list_from_num_1(10, []) == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    assert recursion.get_list_from_num_1(1, []) == [1]
    assert recursion.get_list_from_num_1(0, []) == []

    l = []
    assert recursion.get_list_from_1_num(5, l) == [5, 4, 3, 2, 1][::-1]
    l.clear()
    assert recursion.get_list_from_1_num(10, l) == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1][::-1]
    l.clear()
    assert recursion.get_list_from_1_num(1, l) == [1][::-1]
    l.clear()
    assert recursion.get_list_from_1_num(0, l) == [][::-1]

    assert recursion.factorial(5) == 120
    assert recursion.factorial(3) == 6
    assert recursion.factorial(0) == 0
    assert recursion.factorial(1) == 1

    assert recursion.Palindrome(121) == 121
    assert recursion.Palindrome(123) != 123
    assert recursion.Palindrome(0) == 0
    assert recursion.Palindrome(1) == 1
    assert recursion.Palindrome(11) == 11
    assert recursion.Palindrome(12) == 21
