import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

import basics


def basics_test():
    assert basics.basics_of_python() == True
    print("test_basics---PASS")
   
def type_test():
    a = 10
    assert basics.type_of_variable(a) == int
    a = '10'
    assert basics.type_of_variable(a) == str
    assert basics.type_of_variable(int(a)) == int   
    a = []
    assert basics.type_of_variable(a) == list
    a = {}
    assert basics.type_of_variable(a) == dict
    a = ()
    assert basics.type_of_variable(a) == tuple    
    a = {"apple"}
    assert basics.type_of_variable(a) == set
    a = {"fruit":"apple"}
    assert basics.type_of_variable(a) == dict
    a = 1.1
    assert basics.type_of_variable(a) == float
    a = 6
    assert basics.type_of_variable(str(a)) == str
   
def length_test():
    a = '10'
    assert basics.basics_of_length(a) == 2
    a = []
    assert basics.basics_of_length(a) == 0
    a = ()
    assert basics.basics_of_length(a) == 0
    a = {}
    assert basics.basics_of_length(a) == 0    
    a = [1,2,3]
    assert basics.basics_of_length(a) == 3    
    a = (1,2,3)
    assert basics.basics_of_length(a) == 3
    mydict = {
                "brand": "Ford",
                "model": "Mustang",
                "year": 1964
            }
    assert basics.basics_of_length(mydict) == 3
    mystr = "Python"
    assert basics.basics_of_length(mystr) == 6
    myvar = 12345
    assert basics.basics_of_length(str(myvar)) == 5

def basics_of_cast_test():
    var = 123
    assert basics.basics_of_cast(var, "int") == 123
    assert basics.basics_of_cast(var, "float") == 123.0
    assert basics.basics_of_cast(var, "str") == "123"
    assert basics.basics_of_cast(var, "bool") == True
    
    var = "123"
    assert basics.basics_of_cast(var, "int") == 123
    assert basics.basics_of_cast(var, "float") == 123.0
    assert basics.basics_of_cast(var, "str") == "123"
    assert (basics.basics_of_cast(var, "bool")) == True

    var = 123.0
    assert basics.basics_of_cast(var, "int") == 123
    assert basics.basics_of_cast(var, "float") == 123.0
    assert basics.basics_of_cast(var, "str") == "123.0"
    assert basics.basics_of_cast(var, "bool") == True

    var = 0
    assert basics.basics_of_cast(var, "int") == 0
    assert basics.basics_of_cast(var, "float") == 0.0
    assert basics.basics_of_cast(var, "str") == "0"
    assert basics.basics_of_cast(var, "bool") == False

def basics_of_keywords_test():
    assert basics.basics_of_keywords('var') == False
    assert basics.basics_of_keywords("def") == True
    assert basics.basics_of_keywords('False') == True
    assert basics.basics_of_keywords('yield') == True    
    assert basics.basics_of_keywords('yield1') == (not True)    

def basics_of_enumarate():
    l = [1, 2, 3, 4, 5]
    d = {}
    for index,item in enumerate(l):
        d[f"{index}"] = item
    assert d["0"] == 1
    assert d["1"] == 2
    assert d["2"] == 3
    assert d["3"] == 4
    assert d["4"] == 5

def test_python_basics():

    basics_test()
    type_test()
    length_test()
    basics_of_cast_test()
    basics_of_keywords_test()
