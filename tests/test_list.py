import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from mylist import MyList


def test_list():
    my_list = MyList()
    my_list.AddItem(1)
    my_list.AddItem('1')
    my_list.AddItem(1.0)
    assert my_list.getList() == [1, '1', 1.0]
    assert my_list.getSlicedList1(1,2) == ['1']
    assert my_list.getSlicedList1(1,3) == ['1', 1.0]    
    assert my_list.getSlicedList2(1) == ['1', 1.0]
    assert my_list.getSlicedList2(0) == [1, '1', 1.0]    

    my_list.clearList()
    assert my_list.getList() == []

    my_list.concateList([1, 2, 3, 4, 5])
    assert my_list.getList() == [1, 2, 3, 4, 5]

    my_list.editList(4, 6)
    assert my_list.getList() == [1, 2, 3, 4, 6]    

    my_list.editList(4, 5)
    assert my_list.getList() == [1, 2, 3, 4, 5]

    my_list.deleteItem(4)
    assert my_list.getList() == [1, 2, 3, 4]

    my_list.deleteItem(0)
    assert my_list.getList() == [2, 3, 4]

    my_list.clearList()
    assert my_list.getList() == []
    assert my_list.lenOfList() == 0

    my_list.AddItem('Hi!')
    assert my_list.lenOfList() == 1
    
    my_list.repetitionOfList(3)
    assert my_list.lenOfList() == 3
    assert my_list.getList() == ['Hi!', 'Hi!', 'Hi!']

    assert my_list.findItem('Hi!') == True
    assert my_list.findItem('Hii') == False
    assert my_list.findItem(4) == False

    assert my_list.iterateList() == my_list.getList()
    assert my_list.iterateList() == ['Hi!', 'Hi!', 'Hi!']

    my_list.clearList()
    assert my_list.getList() == []

    my_list.concateList([1, 2, 3, 4, 5])
    assert my_list.getList() == [1, 2, 3, 4, 5]
    
    assert my_list.getValueAt(2) == 3
    assert my_list.getValueAt(-2) == 4
    assert my_list.getValueAt(-1) == 5
    assert my_list.getSlicedList2(-1) == [5]
    assert my_list.getSlicedList2(-3) == [3, 4, 5]
    assert my_list.getSlicedList2(-5) == [1, 2, 3, 4, 5]
    assert my_list.getSlicedList2(-10) == [1, 2, 3, 4, 5]

    l = my_list.getList()
    assert l != []
    assert l == [1, 2, 3, 4, 5]
    assert len(l) == 5
    assert min(l) == 1 # For this API all should be same type
    assert max(l) == 5 # For this API all should be same type
    t = (1, 2, 3, 4, 5)
    assert list(t) == l # converts tuple to list

    l.append(5) # append 5 to list
    assert l.count(5) == 2 # number of time 5 will appear
    assert l.count(3) == 1
    l.extend(t) # extends list with tuple
    assert l == [1, 2, 3, 4, 5, 5, 1, 2, 3, 4, 5]

    assert l.index(5) == 4 # Returns the lowest index in list that obj - 5 appears
    l.insert(0, 0)
    assert l == [0, 1, 2, 3, 4, 5, 5, 1, 2, 3, 4, 5]

    assert l.pop() == 5 # Removes and returns last object or obj from list
    assert l == [0, 1, 2, 3, 4, 5, 5, 1, 2, 3, 4]

    assert l.pop(3) == 3 # Removes and returns first apperared object or obj from list
    assert l == [0, 1, 2, 4, 5, 5, 1, 2, 3, 4]
    
    l.remove(0) #Removes object obj from list
    assert l == [1, 2, 4, 5, 5, 1, 2, 3, 4]

    l.reverse() # Reverses objects of list in place
    assert l == [4, 3, 2, 1, 5, 5, 4, 2, 1]

    l.sort() # Arg: func - Sorts objects of list, use compare func if given
    assert l == [1, 1, 2, 2, 3, 4, 4, 5, 5]

    assert list(set(l)) == [1, 2, 3, 4, 5] # remove duplicates using set and type case to list from set
    l = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5]
    assert len(l) == 11

    ret = []
    for x in l:
        if x not in ret:
            ret.append(x)

    assert ret == [1, 2, 3, 4, 5]    

    [ret.append(x) for x in l if x not in l]
    assert ret == [1, 2, 3, 4, 5]

    l1 = [1, 2, 3]
    l2 = [4]
    assert min(l1, l2) == l1
    assert max(l1, l2) == l2

    l1.clear() # clear all elements in l1
    assert l1 == []
    del l1 # delete instance of l1, Dont do compare or test l1 after this, exception will rise

    l1 = [1, 2, 3, 4] # reverse a list
    assert l1[::-1] == [4, 3, 2, 1]
    assert l1[::-2] == [4, 2]
    assert l1[::-3] == [4, 1]
    assert l1[::-4] == [4]
    assert l1[::-5] == [4]
    assert l1[::1] == [1, 2, 3, 4]
    assert l1[::2] == [1, 3]
    assert l1[::3] == [1, 4]
    assert l1[::4] == [1]
    assert l1[::5] == [1]

    s = "1234"
    assert list(s) == ['1', '2', '3', '4']
    assert list(s) != [1, 2, 3, 4]

    my_list = MyList()
    my_list.AddItem(1)
    my_list.AddItem(2)
    my_list.AddItem(3)
    my_list.AddItem(4)
    my_list.AddItem(5)
    my_list.AddItem(6)
    assert my_list.average() == 3.5
    my_list.concateList([7, 8, 9, 10])
    even,odd = my_list.get_even_odd()
    assert even == [2, 4, 6, 8, 10]
    assert odd == [1, 3, 5, 7, 9]
