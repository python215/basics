from io import SEEK_SET
import os

def test_builtinmethods():

    """
    The abs() is one of the most popular Python built-in functions, which returns the absolute value of a number.
    """
    assert abs(-7) == 7
    assert abs(7) == 7
    assert abs(0) == 0
    assert abs(-0) == 0
    assert abs(1) == 1
    assert abs(True) == 1
    assert abs(False) == 0
    assert abs(-False) == 0

    """
    The all() function returns True if all items in an iterable(list, tuple, dictionary) are true, otherwise it returns False.
    If the iterable object is empty, the all() function also returns True.
    """
    assert all({'*','',''}) == False
    assert all({',',',', ','}) == True

    mylist = [True, True, True]
    assert all(mylist) == True

    mylist = [0, 1, 1]
    assert all(mylist) == False

    mylist = [1, 1, 1]
    assert all(mylist) == True

    mytuple = (0, True, False)
    assert all(mytuple) == False

    myset = {0, 1, 0}
    assert all(myset) == False

    # When used on a dictionary, the all() function checks if all the keys are true, not the values.
    mydict = {0 : "Apple", 1 : "Orange"}
    assert all(mydict) == False

    assert all({}) == True
    assert all(()) == True
    assert all([]) == True

    """
    The any() function returns True if any item in an iterable are true, otherwise it returns False.
    If the iterable object is empty, the any() function will return False.
    """
    assert any({}) == False
    assert any(()) == False
    assert any([]) == False

    assert any({1}) == True
    assert any({0}) == False
    assert any({0,1}) == True
    assert any({0,1,0}) == True    

    assert any((1,)) == True
    assert any((0,)) == False
    assert any((0,1)) == True
    assert any((0,1,0)) == True    
    assert any((True,)) == True
    assert any((False,)) == False
    assert any((False,True)) == True
    assert any((False,1,False)) == True 

    """
    ascii()
    """
    assert ascii('A') == "'A'"
    assert ascii(56) == '56'
    assert ascii('ș') == "'\\u0219'"
    assert ascii('@') == "'@'"
    assert ascii('#') == "'#'"
    assert ascii('~') == "'~'"
    assert ascii('`') == "'`'"
    assert ascii('$') == "'$'"

    """bin() converts an integer to a binary string
    """
    assert bin(7) == '0b111'
    assert bin(-7) == '-0b111'
    assert bin(0) == '0b0'
    assert bin(1) == '0b1'

    """bool() converts a value to Boolean.
    """
    assert bool(0) == False
    assert bool(1) == True
    assert bool(100) == True
    assert bool('gopi') == True
    assert bool([]) == False
    assert bool([1]) == True
    assert bool(()) == False
    assert bool((1)) == True
    assert bool({}) == False
    assert bool({1:'g'}) == True
    assert bool([1].clear()) == False
    assert bool(True) == True
    assert bool(False) == False

    """bytearray
    """
    a = bytearray(3)
    a[0] = 1
    a.append(1)
    assert a == b'\x01\x00\x00\x01'
    a.clear()
    assert a == b''
    
    """bytes() returns an immutable bytes object.
    """
    a = bytes(5)
    assert a == b'\x00\x00\x00\x00\x00'
    a = bytes([1,2,3,4,5])
    assert a[0] == 1
    assert a[4] == 5
    a = bytes([1,2,3,4,1])
    a.count(1) == 2
    a.count(2) == 1

    """callable() tells us if an object can be called.
    """
    assert callable([1,2,3]) == False
    assert callable(list) == True

    """chr() Built In function returns the character in python for an ASCII value.
    """
    assert chr(65) == 'A'
    assert chr(31) == '\x1f'
    assert chr(35) == '#'
    assert chr(36) == '$'
    assert chr(37) == '%'
    assert chr(38) == '&'
    assert chr(44) == ','
    assert chr(49) == '1'

    """classmethod() returns a class method for a given method.
    """
    class fruit:
        def sayhi(self):
            return "Hi, I'm a fruit"

    fruit.sayhi=classmethod(fruit.sayhi)
    assert fruit.sayhi() == "Hi, I'm a fruit"

    """divmod() in Python built-in functions, takes two parameters, and returns a tuple of their quotient and remainder.
    """
    assert divmod(3,7) == (0, 3)
    assert divmod(7,3) == (2, 1)

    """The enumerate() function takes a collection (e.g. a tuple) and returns it as an enumerate object.
    """
    """eval() This Function takes a string as an argument, which is parsed as an expression.
    """
    x = 7
    assert eval('x+7') == 14
    assert divmod(7,3) == (2, 1)

    """float()
    This Python Built In function converts an int or a compatible value into a float.
    """
    assert float(2) == 2.0
    assert float(2.0) == 2.0
    assert float(0) == 0.0
    assert float(True) == 1.0
    assert float(False) == 0.0

    """hash
    """
    assert hash(True) == 1
    assert hash(False) == 0

    """Hex() Python built-in functions, converts an integer to hexadecimal.
    """
    assert hex(16) == '0x10'
    assert hex(True) == '0x1'
    assert hex(False) == '0x0'

    """int()
    """
    assert int(2) == 2
    assert int('2') == 2
    assert int(True) == 1
    assert int(False) == 0
    assert int(2.5) == 2
    assert int(2.999999) == 2

    """isinstance()
    """
    assert isinstance(5, int) == True
    assert isinstance(5, float) == False
    assert isinstance("Hello", (float, int, str, list, dict, tuple)) == True
    
    class myObj:
        name = "John"

    y = myObj()
    assert isinstance(y, myObj) == True

    """issubclass()
    """
    class myAge:
        age = 36

    class myObj(myAge):
        name = "John"
        age = myAge

    assert issubclass(myObj, myAge) == True

    """list(), len()
    """
    assert list({1,3,2,2}) == [1,2,3]
    assert len({1,2,2,3}) == 3
    assert len([1,2,2,3]) == 4

    """max()
    """
    assert max(2,3,10) == 10
    assert max([2,3,5]) == 5

    """next(iterator)
    """
    myIterator = iter([1,2,3,4,5])
    assert next(myIterator) == 1
    assert next(myIterator) == 2
    assert next(myIterator) == 3
    assert next(myIterator) == 4
    assert next(myIterator) == 5

    """object()
    """
    o = object()
    assert type(o) == object

    """oct()
    """
    assert oct(7) == '0o7'
    assert oct(10) == '0o12'
    assert oct(8) == '0o10'
    assert oct(True) == '0o1'
    assert oct(False) == '0o0'

    """pow() takes two arguments- say, x and y. It then returns the value of x to the power of y.
    """
    assert pow(2,2) == 4
    assert pow(2,3) == 8
    
    """file operations
    """
    f = open("test.log", 'a+')
    assert f.write("HELLO") == 5
    f.close()

    f = open("test.log", 'r+')
    assert f.read() == "HELLO"
    f.close()

    """range()
    """
    assert list(range(0,5)) == [0,1,2,3,4]
    assert list(range(1)) == [0]
    assert list(range(2)) == [0, 1]
    assert list(range(0,5,1)) == [0, 1, 2, 3, 4]
    assert list(range(0,5,2)) == [0, 2, 4]
    assert list(range(0,5,3)) == [0, 3]
    assert list(range(0,5,4)) == [0, 4]
    assert list(range(0,5,5)) == [0]
    assert list(range(0,5,6)) == [0]

    """repr() returns a representable string of an object.
    """
    assert repr(True) == 'True'
    assert repr(False) == 'False'
    assert repr(7) == '7'
    assert repr('7') == "'7'"
    assert repr(2.0) == '2.0'

    """reversed()
    """
    iTer = reversed([1,2,3,4])
    l = []
    for item in iTer:
        l.append(item)
        
    assert l == [4,3,2,1]

    """round() rounds off a float to the given number of digits (given by the second argument).
    """
    assert round(3.1423) == 3
    assert round(3.1423,1) == 3.1
    assert round(3.18,1) == 3.2
    assert round(3.11,1) == 3.1
    assert round(3.12345,2) == 3.12
    assert round(3.128,2) == 3.13
    assert round(377.77,-1) == 380.0
    assert round(377.00,-1) == 380.0
    assert round(377.99,-1) == 380.0
    assert round(378,-1) == 380
    assert round(378,-1) == 380
    assert round(379,-1) == 380
    assert round(380,-1) == 380
    assert round(389,-1) == 390
    assert round(387,-1) == 390
    assert round(381,-1) == 380
    assert round(382,-1) == 380
    assert round(383,-1) == 380
    assert round(384,-1) == 380
    assert round(385,-1) == 380
    assert round(386,-1) == 390
    assert round(389,-1) == 390

    """set() returns a set of the items passed to it.
    """
    assert set([1,2,3,4,1]) == {1,2,3,4}

    """setattribute()
    """
    class Person:
        name = "John"
        age = 36
        country = "Norway"

    setattr(Person, 'age', 40)
    # The age property will now have the value: 40
    assert getattr(Person, 'age') == 40

    """slice()
    """
    a = ("a", "b", "c", "d", "e", "f", "g", "h")
    x = slice(2)
    assert a[x] == ('a', 'b')
    x = slice(3)
    assert a[x] == ('a', 'b', 'c')
    x = slice(3,5)
    assert a[x] == ('d', 'e')
    x = slice(0,len(a),2)
    assert a[x] == ('a', 'c', 'e', 'g')

    """sorted() prints out a sorted version of an iterable. It does not, however, alter the iterable.
    """
    assert sorted('Python') == ['P', 'h', 'n', 'o', 't', 'y']
    assert sorted([1,3,2]) == [1, 2, 3]

    """
    str() takes an argument and returns the string equivalent of it.
    """
    assert str(2) == '2'
    assert str('Hi') == 'Hi'
    assert str(2.9) == '2.9'
    assert str(True) == 'True'
    assert str(False) == 'False'
    assert str([1,2,3]) == '[1, 2, 3]'

    """The function sum() takes an iterable as an argument, and returns the sum of all values.
    """
    assert sum([1],2) == 3
    assert sum([1,2],2) == 5
    assert sum([1,2,3],2) == 8

    """super()
    """
    class person:
        def __init__(self):
            print("A person")
    class student(person):
        def __init__(self):
            super().__init__()
            print("A student")
    Avery=student() # A personA student

    """the function tuple() lets us create a tuple.
    """
    assert tuple([1,2,3]) == (1, 2, 3)
    assert tuple([]) == ()
    assert tuple({1:'a',2:'b'}) == (1, 2)

    """type()
    """
    assert type({}) == dict
    assert type([]) == list
    assert type(()) == tuple
    assert type("gre") == str
    assert type(4) == int

    """vars() function returns the __dict__ attribute of a class.
    """
    """zip() returns us an iterator of tuples.
    """
    assert set(zip([1,2,3],['a','b','c'])) == {(2, 'b'), (3, 'c'), (1, 'a')}
    x,y,z = zip([1,2,3],['a','b','c'])
    assert x == (1, 'a')
    assert y == (2, 'b')
    assert z == (3, 'c')
