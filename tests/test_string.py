def test_string():
    s = '' # empty string
    s = "" # empty string
    assert s == ''

    # different ways of representing a string
    s = 'Gopi Prasad'
    assert s[::] == 'Gopi Prasad'
    assert s[:] == 'Gopi Prasad'
    assert s[0:] == 'Gopi Prasad'
    assert s[-11:] == 'Gopi Prasad'
    assert s[-(len(s)):] == 'Gopi Prasad'
    assert s[::1] == 'Gopi Prasad'

    # forward slicing
    assert s[1:] == 'opi Prasad'
    assert s[2:] == 'pi Prasad'
    assert s[4:] == ' Prasad'
    assert s[10:] == 'd'
    assert s[11:] == ''
    assert s[12:] == ''

    assert s[1] == 'o'
    assert s[1:2] == 'o'
    assert s[1:6] == 'opi P'
    assert s[1:len(s)] == 'opi Prasad'
    assert s[1:len(s)+1] == 'opi Prasad'

    assert s[1::1] == 'opi Prasad'
    assert s[0:len(s):1] == 'Gopi Prasad'
    assert s[0::2] == 'Gp rsd'
    assert s[-(len(s))::1] == 'Gopi Prasad'

    # reverse (negative indexing)
    assert s[::-1] == 'dasarP ipoG'
    assert s[::-2] == 'dsr pG'

    # update string
    assert s[5:] + ' Ravada' == 'Prasad Ravada'
    assert s[:4] + ' Ravada' == 'Gopi Ravada'

    # formating - latest
    s1 = 'Gopi '
    s2 = 'Prasad '
    s3 = 'Ravada'
    s = 'Gopi Prasad Ravada'
    assert s == s1 + s2 + s3
    assert 'Gopi' + ' ' + 'Prasad' + ' ' + 'Ravada' == s
    assert f'{s1}{s2}{s3}' == s
    assert f'Hello {s1}{s2}{s3}' == 'Hello ' + s

    # formating - old
    assert "Hello %s%s%s"%(s1, s2, s3) == 'Hello Gopi Prasad Ravada'

    # String Special Operators
    assert "Gopi"+" "+"Ravada" == "Gopi Ravada"
    assert "Gopi"*2 == "GopiGopi"
    assert "Gopi"[0] == 'G'
    assert "Gopi"[:] == "Gopi"
    assert "Gopi"[1:4] == "opi"
    assert ("Gopi" in "1234Gopi5678") == True
    #assert "Gopi" in "1234Gopi5678" == False # PLEASE MAKE A NOTE FOR OPEN/CLOSE PARANTHESIS

    """ On python interpreter
    assert "Gopi\t"
    'Gopi\t'
    >>> r"Gopi\t"
    'Gopi\\t'
    >>> r"Gopi\n"
    'Gopi\\n'
    >>> "Gopi\n"
    'Gopi\n'
    >>> print("Gopi\n")
    Gopi

    >>> print(r"Gopi\n")
    Gopi\n
    >>> "Gopi\n"
    'Gopi\n'
    >>>   

    Different Specifiers
    Format Symbol	Conversion
    %c	character
    %s	string conversion via str() prior to formatting
    %i	signed decimal integer
    %d	signed decimal integer
    %u	unsigned decimal integer
    %o	octal integer
    %x	hexadecimal integer (lowercase letters)
    %X	hexadecimal integer (UPPERcase letters)
    %e	exponential notation (with lowercase 'e')
    %E	exponential notation (with UPPERcase 'E')
    %f	floating point real number
    %g	the shorter of %f and %e
    %G	the shorter of %f and %E
    """

    """
	capitalize()
    Capitalizes first letter of string
    """
    assert "gopi".capitalize() == "Gopi"

    """
    center(width, fillchar)
    Returns a space-padded string with the original string centered to a total of width columns
    """
    assert "gopi".center(10, ' ') == '   gopi   '
    assert "gopi".center(11, ' ') == '    gopi   '

    """
    ljust(width[, fillchar])
    Returns a space-padded string with the original string left-justified to a total of width columns.
    """    
    assert "gopi".ljust(10, ' ') == "gopi      "

    """
    count(str, beg= 0,end=len(string))
    Counts how many times str occurs in string or in a substring of string if starting index beg and ending index end are given.
    """
    assert "gopigopigopi".count('gop') == 3 
    assert "gopigopigopi".count('gopi', 1, 20) == 2 # as first gopi skipped by second argument 

    """
    encode(encoding='UTF-8',errors='strict')
    Returns encoded string version of string; on error, default is to raise a ValueError unless errors is given with 'ignore' or 'replace'.

    decode(encoding='UTF-8',errors='strict')
    Decodes the string using the codec registered for encoding. encoding defaults to the default string encoding.
    """
    """
    exception:
    LookupError: 'base64' is not a text encoding; use codecs.encode() to handle arbitrary codecs
    test_string.py:134: LookupError
    """
    # Str = "this is string example....wow!!!"
    # Str = Str.encode('base64', 'strict')

    # assert Str == 'dGhpcyBpcyBzdHJpbmcgZXhhbXBsZS4uLi53b3chISE='
    # assert Str.decode('base64', 'strict') == "this is string example....wow!!!"

    """
    endswith(suffix, beg=0, end=len(string))
    Determines if string or a substring of string (if starting index beg and ending index end are given) ends with suffix; returns true if so and false otherwise.
    """
    assert "gopi".endswith('i') == True
    assert "gopi".endswith('p') == False

    """
    expandtabs(tabsize=8)
    Expands tabs in string to multiple spaces; defaults to 8 spaces per tab if tabsize not provided.
    """

    # TBD

    """
    find(str, beg=0 end=len(string))
    Determine if str occurs in string or in a substring of string if starting index beg and ending index end are given returns index if found and -1 otherwise.
    """
    assert "Gopi PRasad".find('Gopi') == 0
    assert "Gopi PRasad".find('Pr') == -1
    assert "Gopi PRasad".find('PR') == 5
    assert "Gopi PRasad".find('PR', 5) == 5
    assert "Gopi PRasad".find('PR', 6) == -1
    assert "Gopi PRasad".find('Gopi', 1) == -1

    """
    index(str, beg=0, end=len(string))
    Same as find(), but raises an exception if str not found.
    """
    assert "Gopi PRasad".index('Gopi') == 0
    assert "Gopi PRasad".index('PR') == 5
    assert "Gopi PRasad".index('PR', 5) == 5

    """
    isalnum()
    Returns true if string has at least 1 character and all characters are alphanumeric and false otherwise.
    """
    assert "gopi".isalnum() == True
    assert "gopi123".isalnum() == True
    assert "gopi123 ".isalnum() == False

    """
    isalpha()
    Returns true if string has at least 1 character and all characters are alphabetic and false otherwise.
    """
    assert "gopi".isalpha() == True
    assert "gopi123".isalpha() == False
    assert "gopi123 ".isalpha() == False    

    """
    isdigit()
    Returns true if string contains only digits and false otherwise.
    """
    assert "gopi".isdigit() == False
    assert "gopi1".isdigit() == False
    assert "g123".isdigit() == False
    assert "123".isdigit() == True
    assert " ".isdigit() == False
    assert "".isdigit() == False
    assert "\n".isdigit() == False

    """
    isnumeric()
    Returns true if a unicode string contains only numeric characters and false otherwise.
    """
    assert "gopi".isnumeric() == False
    assert "123G".isnumeric() == False
    assert "123".isnumeric() == True
    assert u"123".isnumeric() == True
    assert U"123".isnumeric() == True

    """
	islower()
    Returns true if string has at least 1 cased character and all cased characters are in lowercase and false otherwise.    

	isupper()
    Returns true if string has at least one cased character and all cased characters are in uppercase and false otherwise    
    """
    s = "Gopi"
    assert s.islower() == False
    assert s.isupper() == False
    s = "GOPI"
    assert s.isupper() == True
    assert s.islower() == False
    assert "GOPI".lower().islower() == True
    assert "GOPI".lower().upper().islower() == False
    assert "GOPI".lower().upper().isupper() == True

    """
    isspace()
    Returns true if string contains only whitespace characters and false otherwise.
    """
    assert " ".isspace() == True
    assert "Gopi Prasad".isspace() == False
    assert "       ".isspace() == True
    assert "".isspace() == False
    assert "\t".isspace() == True

    """
    istitle()
    Returns true if string is properly "titlecased" and false otherwise.
    """
    assert " ".istitle() == False
    assert "gopi prasad ravada".istitle() == False
    assert "gopi prasad Ravada".istitle() == False
    assert "gopi Prasad Ravada".istitle() == False
    assert "Gopi Prasad Ravada".istitle() == True
    assert "Gopi Prasad rAavada".istitle() == False

    """
    join(seq)
    Merges (concatenates) the string representations of elements in sequence seq into a string, with separator string.
    """
    assert "-".join(("a", "b", "c")) == "a-b-c"
    assert " ".join(("Gopi", "Prasad", "Ravada")) == "Gopi Prasad Ravada"

    """
    len(string)
    Returns the length of the string
    """
    assert len('') == 0
    assert len(' ') == 1
    assert len('\t') == 1
    assert len('     1') == 6
    assert len('Hello World!') == 12

    """	
    ljust(width[, fillchar])
    Returns a space-padded string with the original string left-justified to a total of width columns.
    """
    assert "gopi".ljust(10, ' ') == 'gopi      '
    assert "gopi".ljust(10, ' ') != "gopi     "

    """
    lstrip() - str.lstrip([chars])
    Removes all leading whitespace in string.

    rstrip()
    Removes all trailing whitespace of string.
    """
    assert "   Gopi   Prasad    Ravada   ".lstrip() == 'Gopi   Prasad    Ravada   '
    assert "1234Gopi   Prasad    Ravada   ".lstrip("1234") == 'Gopi   Prasad    Ravada   '
    assert "111111Gopi   Prasad    Ravada   ".lstrip("1") == 'Gopi   Prasad    Ravada   '

    assert "   Gopi   Prasad    Ravada   ".rstrip() == '   Gopi   Prasad    Ravada'
    assert "Gopi   Prasad    Ravada1234".lstrip("1") == 'Gopi   Prasad    Ravada1234'
    assert "Gopi   Prasad    Ravada1234".lstrip("1234") == 'Gopi   Prasad    Ravada1234'
    assert "Gopi   Prasad    Ravada1234".rstrip("1234") == 'Gopi   Prasad    Ravada'
    assert "Gopi   Prasad    Ravada11111".rstrip("1") == 'Gopi   Prasad    Ravada'
    assert "Gopi   Prasad    Ravada1 ".rstrip("1 ") == 'Gopi   Prasad    Ravada'

    """
    maketrans()
    Returns a translation table to be used in translate function.
    from string import maketrans   # Required to call maketrans function.

    intab = "aeiou"
    outtab = "12345"
    trantab = maketrans(intab, outtab)

    str = "this is string example....wow!!!"
    print str.translate(trantab)
    """

    """
    max(str)
    Returns the max alphabetical character from the string str.

    25	min(str)
    Returns the min alphabetical character from the string str.
    """
    assert min("azxwq") == 'a'
    assert max("azxwq") == 'z'
    assert max("azxwq91") == 'z'
    assert min("azxwq123") == '1'
    assert min("azxwq1230") == '0'
    assert min("azxwq1230 ") == ' '
    assert min("azxwq1230\n") == '\n'
    assert min("azxwq1230\r") == '\r'
    assert min("azxwq1230\n\r") == '\n'

    """
    replace(old, new [, max])
    Replaces all occurrences of old in string with new or at most max occurrences if max given.
    """
    assert "Gopi Prasad Ravada".replace('Gopi', '') == " Prasad Ravada"
    assert "Gopi Prasad ravada".replace('ra', '', 3) == 'Gopi Psad vada'
    assert "Gopi Prasad Ravada".replace('ra', '', 3) == 'Gopi Psad Ravada'

    """
    rfind(str, beg=0,end=len(string))
    Same as find(), but search backwards in string.
    """
    str1 = "this is really a string example....wow!!!"
    str2 = "is"

    assert str1.rfind(str2) == 5
    assert str1.rfind(str2, 0, 10) == 5
    assert str1.rfind(str2, 10, 0) == -1

    assert str1.find(str2) == 2
    assert str1.find(str2, 0, 10) == 2
    assert str1.find(str2, 10, 0) == -1

    """
    rindex( str, beg=0, end=len(string))
    Same as index(), but search backwards in string.
    """
    str1 = "this is string example....wow!!!"
    str2 = "is"

    assert str1.rindex(str2) == 5
    assert str1.index(str2) == 2
    
    """
    rjust(width,[, fillchar])
    Returns a space-padded string with the original string right-justified to a total of width columns.
    """
    str1 = "this is string example....wow!!!"
    assert str1.rjust(50, '0') == '000000000000000000this is string example....wow!!!'
    assert str1.ljust(50, '0') == 'this is string example....wow!!!000000000000000000'

    """
	split(str="", num=string.count(str))
    Splits string according to delimiter str (space if not provided) and returns list of substrings; split into at most num substrings if given.    
    """
    line = '#define RELEASE_VERSION "2.0.0"'
    s1, s2, s3 = line.split(' ')
    assert s1 == '#define'
    assert s2 == 'RELEASE_VERSION'
    assert s3 == '"2.0.0"'

    """
    strip([chars])
    Performs both lstrip() and rstrip() on string.
    """
    assert s3.strip("\"") == '2.0.0' 

    """
    splitlines( num=string.count('\n'))
    Splits string at all (or num) NEWLINEs and returns a list of each line with NEWLINEs removed.
    """
    str1 = "Line1-a b c d e f\nLine2- a b c\n\nLine4- a b c d"
    assert str1.splitlines() == ['Line1-a b c d e f', 'Line2- a b c', '', 'Line4- a b c d']
    assert str1.splitlines(0) == ['Line1-a b c d e f', 'Line2- a b c', '', 'Line4- a b c d']
    assert str1.splitlines(3) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']
    assert str1.splitlines(4) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']
    assert str1.splitlines(5) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']

    str1 = "Line1-a b c d e f\nLine2- a b c\n\nLine4- a b c d"
    assert str1.splitlines(True) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']
    assert str1.splitlines( 0 ) == ['Line1-a b c d e f', 'Line2- a b c', '', 'Line4- a b c d']
    assert str1.splitlines( 3 ) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']
    assert str1.splitlines( 4 ) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']
    assert str1.splitlines( 5 ) == ['Line1-a b c d e f\n', 'Line2- a b c\n', '\n', 'Line4- a b c d']

    """
    startswith(str, beg=0,end=len(string))
    Determines if string or a substring of string (if starting index beg and ending index end are given) starts with substring str; returns true if so and false otherwise.
    """
    assert "Gopi".startswith('g') == False
    assert "Gopi".startswith('G') == True
    assert "Gopi".startswith('G', 1) == False
    assert "Gopi".startswith('G', 0) == True
    assert "Gopi".startswith('G', 0, 100) == True
    assert "Gopi".startswith('0', 1, 100) == False
    assert "Gopi".startswith('o', 1, 100) == True
    assert " Gopi".startswith(" ") == True

    """
    swapcase()
    Inverts case for all letters in string.
    """
    assert 'this is string example....wow!!!'.swapcase() == 'THIS IS STRING EXAMPLE....WOW!!!'
    assert " ".swapcase() == ' '
    assert "  i ".swapcase() == '  I '

    """
    title()
    Returns "titlecased" version of string, that is, all words begin with uppercase and the rest are lowercase.
    """
    assert "gopi prasad ravada".title() == 'Gopi Prasad Ravada'
    assert "Gopi Prasad Ravada".title() == 'Gopi Prasad Ravada'
    assert "  i ".swapcase() == '  I '

    """
    translate(table, deletechars="")
    Translates string according to translation table str(256 chars), removing those in the del string.
    """
    # TBD

    """
    upper()
    Converts lowercase letters in string to uppercase.
    """
    assert "gopi".lower() == 'gopi'
    assert "Gopi".lower() == 'gopi'
    assert "Gopi".lower().upper() == "GOPI"
    assert "Gopi".lower().upper().lower() == "gopi"

    """
    zfill (width)
    Returns original string leftpadded with zeros to a total of width characters; intended for numbers, zfill() retains any sign given (less one zero).
    """
    assert "Gopi".zfill(1) == 'Gopi'
    assert "Gopi".zfill(14) == '0000000000Gopi'
    assert "Gopi".zfill(4) == 'Gopi'
    assert "Gopi".zfill(5) == '0Gopi'
    assert "Gopi".zfill(0) == 'Gopi'

    """
    isdecimal()
    Returns true if a unicode string contains only decimal characters and false otherwise.
    """
    
    assert u"this2009".isdecimal() == False
    assert u"23443434".isdecimal() == True
