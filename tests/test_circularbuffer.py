import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from circular_buffer import CircularBuffer

def circularbuffer_init():
    cb = CircularBuffer(5)
    assert cb.size() == 5
    print(f"is full: {cb.is_full()}")
    print(f"is empty: {cb.is_empty()}")
    assert cb.is_empty() == True
    assert cb.is_full() == False

    cb.enqueue("one")
    cb.enqueue("two")
    cb.enqueue("three")
    cb.enqueue("four")
    cb.enqueue("five")
    print(f"is full: {cb.is_full()}")
    print(f"is empty: {cb.is_empty()}")
    assert cb.is_empty() == False
    assert cb.is_full() == True
