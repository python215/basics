def fun_with_one_arg(a: int):
    return a + 10

def fun_with_two_arg(a: int, b: int):
    return a + b +10

def fun_with_mixed_Arg_Types(a: int, s: str):
    return s*a

def fun_with_string_arg(in1: str):
    return in1+in1

def fun_with_variable_args(*args):
    l = []
    for arg in args:
        l.append(arg)
    s = ''
    for i in l:
        s += i
    return l, s

def fun_with_keyword_arg(in3, in2, in1):
    return in3, in2, in1

def fun_with_variable_kwargs(**args):
    l = []
    l.append(args['name'])
    l.append(args['age'])
    l.append(args['marks'])
    return l

def fun_with_default_arg(in1 = "Hello"):
    return in1+" World!"

def fun_with_list_arg(list_):
    # return list_.reverse()
    return list_[::-1]

def fun_recursion(val):
    result = 0
    if val > 0:
        result += val + fun_recursion(val-1)
    return result

def fun_factorial(val):
    if val == 0:
        return 1
    return val * fun_factorial(val-1)

def fun_fibonanci(val):
    if val < 0:
        return None
    elif val <= 1:
        return val
    else:
        return fun_fibonanci(val-1) + fun_fibonanci(val-2)

def test_functions():
    assert fun_with_one_arg(10) == 20
    assert fun_with_two_arg(10, 20) == 40
    assert fun_with_mixed_Arg_Types("Hi", 2) == 'HiHi'
    assert fun_with_string_arg("Hi") == 'HiHi'
    l, s = fun_with_variable_args("Gopi", "Prasad", "Ravada")
    assert l == list(("Gopi", "Prasad", "Ravada"))
    assert s == "GopiPrasadRavada"

    in3, in2, in1 = fun_with_keyword_arg(in1 = 1, in2 = 2, in3 = 3)
    assert in1 == 1
    assert in2 == 2
    assert in3 == 3

    assert fun_with_variable_kwargs(marks=60, name="Gopi", age=36) == ['Gopi', 36, 60]

    assert fun_with_default_arg("Python") == "Python World!"
    assert fun_with_default_arg() == "Hello World!"
    assert fun_with_default_arg("") == " World!"

    assert fun_with_list_arg([1, 2, 3]) == [3, 2, 1]

    assert fun_recursion(6) == 21
    assert fun_recursion(0) == 0
    assert fun_recursion(1) == 1

    assert fun_factorial(0) == 1
    assert fun_factorial(1) == 1
    assert fun_factorial(5) == 120

    assert fun_fibonanci(9) == 34
    assert fun_fibonanci(0) == 0
    assert fun_fibonanci(1) == 1
    assert fun_fibonanci(-1) == None
