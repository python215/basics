def test_dict():
    """
    As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.
    Dictionaries are changeable, meaning that we can change, add or remove items after the dictionary has been created.
    Dictionaries cannot have two items with the same key
    """
    d1 = {"brand": "Maruthi", "model": "Swift", "year":2019}
    assert d1["brand"] == 'Maruthi'
    assert d1["model"] == 'Swift'
    assert d1["year"] == 2019
    assert len(d1) == 3

    # update
    d1["brand"] = "Honda"
    d1["model"] = "Amaze"
    d1["year"]  = 2022
    assert d1["brand"] == 'Honda'
    assert d1["model"] == 'Amaze'
    assert d1["year"] == 2022
    assert len(d1) == 3

    # ADD
    d1["colors"] = ["pearl wight", "magma grey", "Blue"]
    assert d1["colors"] == ["pearl wight", "magma grey", "Blue"]
    assert len(d1) == 4

    # type
    assert type(d1) == dict
    assert type(d1["colors"]) == list

    # Access
    assert d1["colors"] == ["pearl wight", "magma grey", "Blue"]
    assert d1.get("brand") == 'Honda'
    assert len(d1['colors']) == 3

    # d1.keys() == dict_keys(['brand', 'model', 'year', 'colors'])
    assert len(d1.keys()) == 4
    assert ('brand' in d1.keys()) == True

    # d1.values() == dict_values(['Hyundai', 'Swift', 2019, ['pearl wight', 'magma grey', 'Blue']])
    assert len(d1.values()) == 4
    assert (2022 in d1.values()) == True

    # d1.items() == dict_items([('brand', 'Hyundai'), ('model', 'Swift'), ('year', 2019), ('colors', ['pearl wight', 'magma grey', 'Blue'])])
    assert (('brand', 'Honda') in d1.items()) == True
    assert len(d1.items()) == 4

    ## Update or change
    d1.update({"brand":"Honda"})
    d1.update({"model":"City"})
    assert d1["brand"] == 'Honda'
    assert d1["model"] == 'City'
    assert d1["year"] == 2022
    assert len(d1["colors"]) == 3
    
    # delete
    d1.pop('colors')
    assert len(d1) == 3

    d1.popitem() # year will be deleted
    assert len(d1) == 2 
    assert d1["brand"] == 'Honda'
    assert d1["model"] == 'City'

    del d1["model"]
    assert len(d1) == 1
    assert d1["brand"] == 'Honda'

    d2 = d1.copy()
    # d1.clear() # exception
    # len(d1) == 0
    # del d1

    assert len(d2) == 1
    assert d2["brand"] == 'Honda'
    del d2

    # loop
    d2 = {}
    d1 = {"name":"gopi", "class":"ece", "year":2006, "marks":67.41}
    for item in d1:
        d2[item] = d1[item]
    assert d1 == d2

    del d2
    d2 = {}
    for key, value in d1.items():
        d2[key] = value
    assert d1 == d2

    del d2
    d2 = {x:y for x,y in d1.items()}
    assert d1 == d2
    del d1
    del d2
    
    # copy/dict()
    dict1 = {"first":1, "second":2, "third":3, "last":0}
    dict2 = dict1.copy()
    assert dict1 == dict2
    del dict1, dict2

    dict1 = {"first":1, "second":2, "third":3, "last":0}
    dict2 = dict(dict1)
    assert dict1 == dict2
    del dict1, dict2

    # nested dicts
    students = {"s1":{"name": 'gopi', "age":36}, "s2":{"name":'prasad', "age":37}, "surname":{"gopi": "ravada", "prasad": "panga"}}
    assert len(students) == 3
    del students

    # nested dicts
    s1 = {"name": 'gopi', "age":36}
    s2 = {"name":'prasad', "age":37}
    surname = {"gopi": "ravada", "prasad": "panga"}
    students = {"s1":s1, "s2":s2, "surname":surname}

    assert len(students) == 3
    assert students["surname"] == {"gopi": "ravada", "prasad": "panga"}
    assert students["s1"]["name"] == 'gopi'
    assert students['surname'][students["s1"]["name"]] == 'ravada'
    del students

    # fromkeys()	Returns a dictionary with the specified keys and value
    k = ('name', 'age', 'marks')
    v = 0
    d = dict.fromkeys(k, v)
    assert len(d) == 3
    assert d['name'] == 0
    assert d['age'] == 0
    assert d['marks'] == 0
    del k, v

    k = ('name', 'age', 'marks')
    d = dict.fromkeys(k)
    assert d['name'] == None
    assert d['age'] == None
    assert d['marks'] == None
    del k, d

    # Returns the value of the specified key. If the key does not exist: insert the key, with the specified value
    k = {'name':'gopi', 'age':26, 'marks':90}
    k.setdefault('name', 'Gopi')
    assert len(k) == 3
    assert k['name'] == 'gopi'
    del k['name']
    assert len(k) == 2
    k.setdefault('name', 'Gopi')
    assert len(k) == 3
    assert k['name'] == 'Gopi'
    del k
