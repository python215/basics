from abc import abstractclassmethod


def test_sets():
    my_set = {}
    assert len(my_set) == 0

    my_set = {"apple", "banana", "cherry", "dragon"}
    assert len(my_set) == 4

    my_set = {True, False, False}    
    assert len(my_set) == 2

    my_set = {"abc", 34, True, 40, "male"}
    assert len(my_set) == 5

    assert type(my_set) == set

    assert set([1, 2, 3, 4]) == {1, 2, 3, 4}
    assert set(['1', '2', '3', '4']) == {'1', '2', '3', '4'}

    thisset = {"apple", "banana", "cherry"}

    for x in thisset:
        print(x)

    my_set = {"apple", "banana", "cherry", "dragon"}
    assert ('banana' in my_set) == True
    assert ('Banana' in my_set) == False
    assert ('mango' in my_set) == False

    my_set.add("mango")
    assert ('mango' in my_set) == True

    set1 = {"apple", "banana", "cherry"}
    set2 = {"pineapple", "mango", "papaya"}
    set1.update(set2)
    print(set1)
    assert ("pineapple" in set1) == True
    assert ("mango" in set1) == True
    assert ("papaya" in set1) == True
    assert ('banana' in set1) == True

    set2 = [1, 2, 3] # updated set may not be set, could be other . ex.list
    set1.update(set2) 
    assert (3 in set1) == True

    # remove
    set2 = {"pineapple", "mango", "papaya"}
    assert len(set2) == 3
    set2.remove('pineapple')
    assert len(set2) == 2
    assert ('pineapple' in set2) == False # if item not in set, it will raise exception

    set2.remove('mango')
    assert len(set2) == 1
    assert ('mango' in set2) == False # if item not in set, it will not raise exception
    
    """
    You can also use the pop() method to remove an item, but this method will remove the last item. Remember that sets are unordered, so you will not know what item that gets removed.
    The return value of the pop() method is the removed item.
    """
    set2.add('mango')
    assert len(set2) == 2
    set2.pop() 
    assert len(set2) == 1
    set2.pop() 
    assert len(set2) == 0

    # clear
    set1.clear()
    assert len(set1) == 0
    assert set1 == set()

    # delete
    set1.add('mango')
    assert len(set1) == 1
    del set1
    # should not access set1 after above step

    # join sets
    set1 = {"a", "b", "c"}
    set2 = {1, 2, 3}

    # Both union() and update() will exclude any duplicate items.
    set3 = set1.union(set2)
    # print(set3)
    assert len(set3) == 6

    set1.update(set2)
    set4 = set1
    # print(set4)
    assert len(set4) == 6

    """
    Keep ONLY the Duplicates
    The intersection_update() method will keep only the items that are present in both sets.
    """
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    x.intersection_update(y)
    assert len(x) == 1 # 'apple'

    z = x.intersection(y)
    assert len(z) == 1 # 'apple'

    """
    Keep All, But NOT the Duplicates
    The symmetric_difference_update() method will keep only the elements that are NOT present in both sets.
    """
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    x.symmetric_difference_update(y)
    assert len(x) == 4

    # The symmetric_difference() method will return a new set, that contains only the elements that are NOT present in both sets.
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    z = x.symmetric_difference(y)
    assert len(z) == 4

    # copy()	Returns a copy of the set
    x = {"apple", "banana", "cherry"}
    y = set()
    y = x.copy()
    assert len(y) == 3

    # difference()	Returns a set containing the difference between two or more sets
    x = {"apple", "banana", "cherry"}
    y = {"microsoft", "apple"}

    z = x.difference(y)    
    assert len(z) == 2
    z = y.difference(x)
    assert len(z) == 1

    # remove : Remove the items that exist in both sets:
    x = {"apple", "banana", "cherry"}
    y = {"microsoft", "apple"}
    x.difference_update(y)
    assert len(x) == 2
    x = {"apple", "banana", "cherry"}
    y = {"microsoft", "apple"}
    y.difference_update(x)
    assert len(y) == 1

    # discard()	Remove the specified item
    s = {"1", "2", "3", "4"}
    s.discard("4")
    assert len(s) == 3
    
    s1 = {"1", "2", "3", "4"}
    s2 = {"4", "2", "0"}
    s3 = s1.intersection(s2)
    s4 = s2.intersection(s1)
    assert len(s3) == 2
    assert len(s4) == 2

    s1.intersection_update(s2)
    assert len(s1) == 2
    s2.intersection_update(s1)
    assert len(s2) == 2

    x = {"a", "b", "c"}
    y = {"c", "d", "e"}
    z = {"f", "g", "c"}

    x.intersection_update(y, z)
    assert len(x) == 1

    x = {"a", "b", "c"}
    y = {"c", "d", "e"}
    z = {"f", "g", "l"}

    x.intersection_update(y, z)
    assert len(x) == 0

    # isdisjoint()	Returns whether two sets have a intersection or not
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "facebook"}

    z = x.isdisjoint(y)
    assert z == True    

    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    z = x.isdisjoint(y)    
    assert z == False

    # issubset()	Returns whether another set contains this set or not
    s1 = {1, 2, 3}
    s2 = {2, 3}
    assert s1.issubset(s2) == False
    assert s2.issubset(s1) == True

    # issuperset()	Returns whether this set contains another set or not
    s1 = {1, 2, 3}
    s2 = {2, 3}
    assert s1.issuperset(s2) == True
    assert s2.issuperset(s1) == False

    # pop()	Removes an element from the set
    s1 = {1, 2, 3}
    s1.pop()
    assert len(s1) == 2
    s1.pop()
    assert len(s1) == 1
    s1.pop()
    assert len(s1) == 0

    # remove()	Removes the specified element
    s1 = {1, 2, 3}
    s1.remove(1)
    assert len(s1) == 2
    s1.remove(2)
    assert len(s1) == 1
    s1.remove(3)
    assert len(s1) == 0

    # symmetric_difference()	Returns a set with the symmetric differences of two sets
    s1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
    s2 = {10, 9, 21, 31, 41, 3, 2}
    s3 = s1.symmetric_difference(s2) # {0, 1, 4, 5, 6, 7, 8, 10, 21, 31, 41}
    s4 = s2.symmetric_difference(s1) # {0, 1, 4, 5, 6, 7, 8, 41, 10, 21, 31}
    assert len(s3) == 11
    assert len(s4) == 11

    # symmetric_difference_update()	inserts the symmetric differences from this set and another
    s1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
    s2 = {10, 9, 21, 31, 41, 3, 2}
    s1.symmetric_difference_update(s2) # {0, 1, 4, 5, 6, 7, 8, 41, 10, 21, 31}
    assert len(s1) == 11
    s1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
    s2 = {10, 9, 21, 31, 41, 3, 2}
    s2.symmetric_difference_update(s1) # {0, 1, 4, 5, 6, 7, 8, 10, 41, 21, 31}
    assert len(s2) == 11

    # union()	Return a set containing the union of sets
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    z = x.union(y)    
    assert len(z) == 5

    x = {"a", "b", "c"}
    y = {"f", "d", "a"}
    z = {"c", "d", "e"}

    result = x.union(y, z)
    assert len(result) == 6 

    # update()	Update the set with the union of this set and others 
    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    x.update(y)    
    assert len(x) == 5

    x = {"apple", "banana", "cherry"}
    y = {"google", "microsoft", "apple"}

    y.update(x)    
    assert len(y) == 5
