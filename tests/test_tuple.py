def test_tuple():
    my_tup = () # empty tuple
    assert my_tup == ()
    
    my_tup += (9,) # comma should be there even single value in tuple
    assert my_tup == (9,)
    
    t = (1, 2, 3)
    my_tup += t
    assert my_tup == (9, 1, 2, 3)

    #my_tup[0] = (0,) # not valid for tuple
    # del tup[0] # removing individuals are not possible for tuple

    my_tup += ()
    assert my_tup == (9, 1, 2, 3)

    assert len(my_tup) == 4 # length
    my_tup += (4, 5, 6) # concatination
    assert my_tup == (9, 1, 2, 3, 4, 5, 6) 
    assert len(my_tup) == 7 # length

    my_tup = (1,)
    my_tup = my_tup*4 # Repetition
    assert my_tup == (1, 1, 1, 1) 

    assert (1 in my_tup) == True
    assert (2 in my_tup) == False

    my_tup = (1, 2, 5, 4, 2) # iterative test
    ret = ()
    for x in my_tup:
        if x not in ret:
            ret += (x,)
    
    assert ret == (1, 2, 5, 4) # if compare index also, then we can make exact copy of my_tup

    assert my_tup[0] == 1 # index value test
    assert my_tup[1] == 2
    assert my_tup[2] == 5
    assert my_tup[3] == 4
    assert my_tup[4] == 2
    assert my_tup[-5] == 1
    assert my_tup[-4] == 2
    assert my_tup[-3] == 5
    assert my_tup[-2] == 4
    assert my_tup[-1] == 2
    assert my_tup[:] == (1, 2, 5, 4, 2)
    assert my_tup[0:] == (1, 2, 5, 4, 2)
    assert my_tup[2:3] == (5,)
    assert my_tup[-1:] == (2,)
    assert my_tup[-5:] == (1, 2, 5, 4, 2)

    (x, y) = (4, 5) # Assignemnt test
    assert x == 4
    assert y == 5

    x, y = 0, 1
    assert x == 0
    assert y == 1

    _, _ = 4, 5
    assert x == 0
    assert y == 1

    _, y = 4, 5
    assert x == 0
    assert y == 5

    x, _ = 4, 6
    assert x == 4 
    assert y == 5

    assert min(my_tup) == 1
    assert max(my_tup) == 5

    l = [1, 2, 3, 4, 5]
    assert tuple(l) == (1, 2, 3, 4, 5)

    t1 = (1, 2, 3)
    t2 = (5,)
    assert min(t1, t2) == t1
    assert max(t1, t2) == t2

    t2 = (0,)
    assert min(t1, t2) == t2
    assert max(t1, t2) == t1

    l1 = tuple([1, 2, 3, 4]) # reverse a list
    assert l1[::-1] == tuple([4, 3, 2, 1])
    assert l1[::-2] == tuple([4, 2])
    assert l1[::-3] == tuple([4, 1])
    assert l1[::-4] == tuple([4])
    assert l1[::-5] == tuple([4])
    assert l1[::1] == tuple([1, 2, 3, 4])
    assert l1[::2] == tuple([1, 3])
    assert l1[::3] == tuple([1, 4])
    assert l1[::4] == tuple([1])
    assert l1[::5] == tuple([1])