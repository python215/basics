
def compare(a: int, b: int):
    if a == b:
        return '='
    elif a < b:
        return '<'
    elif a <= b:
        return '<='
    elif a > b:
        return '>'
    elif a >= b:
        return '>='
    else:
        return'NONE'
    # elif a != b:
    #     return'Not Equals'

def if_elif_else_test():
    assert compare(a=10, b=20) == '<'
    assert compare(a=20, b=20) == '='

    ('True' if 20 < 20 else 'False') == 'False'
    ('True' if 10 < 20 else 'False') == 'True'

    a = 100
    b = 200
    assert ('B' if a < b else '=' if a==b else 'A') == 'B'
    a = 300
    assert ('B' if a < b else '=' if a==b else 'A') == 'A'
    b = 300
    assert ('B' if a < b else '=' if a==b else 'A') == '='

    assert (1 if a == 300 and b == 300 else 0) == 1
    a= 200
    assert (1 if a == 300 and b == 300 else 0) == 0
    assert (1 if a == 300 or b == 300 else 0) == 1
    assert (1 if a == 0 or b == 0 else 0) == 0

def while_test():
    i = 0
    l = []
    while i < 6:
        l.append(i)
        i += 1
    assert l == [0, 1, 2, 3, 4, 5]

    i = 0
    l = []
    while i < 6:
        i += 1
        if i == 5:
            continue
        l.append(i)
    assert l == [1, 2, 3, 4, 6]

    i = 0
    l = []
    while i < 6:
        l.append(i)
        i += 1
    else:
        l.remove(0)
    assert l == [1, 2, 3, 4, 5]

def for_test():
    l = [1, 2, 3, 4, 5, 6]
    for x in l:
        # print(x)
        if x > 4:
            l.remove(x)
        # print(l)
    assert l == [1, 2, 3, 4, 6]
    del l

    l1 = [1, 2, 3, 4, 5, 6]
    l2 = []
    for item in l1:
        l2.append(item)
    assert l1 == l2

    l2.clear()
    [l2.append(x) for x in l1]
    assert l1 == l2

    s1 = "Hello World"
    s2 = ''
    for x in s1: s2 += x
    assert s2 == s1

    l1 = ['g', 'o', 'p', 'i']
    str_ = ""
    for x in l1: str_ += x

    assert str_ == 'gopi'

def test_conditions():
    if_elif_else_test()
    while_test()
    for_test()
