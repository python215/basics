def fun_multiplier(n):
    return lambda a : a*n

def test_lambda():
    
    lambda_cube = lambda y: y*y*y
    assert lambda_cube(2) == 8

    mytripler = fun_multiplier(3)
    assert mytripler(11) == 33
