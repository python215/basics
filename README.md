<div align="center">
    <img alt="python_basics_logo" src=./resources/python-basics.jpeg width="40%">
</div>

# basics
Includes the following concepts
` 
Basic syntax, 
variables and data types,
conditionals,
type casting and exceptions,
functions and built-in functions,
Lists , tuples, sets and dictionaries
`
# References
[**GeeksForGeeks**](https://www.geeksforgeeks.org/python-programming-language/)

[**W3Schools**](https://www.w3schools.com/python/) 

Lists: [**TutorialPoint**](https://www.tutorialspoint.com/python/python_lists.htm)
